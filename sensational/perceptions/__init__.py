from .acceleration import Acceleration
from .magnetism import Magnetism
from .temperature import Temperature
from .depth import Depth
